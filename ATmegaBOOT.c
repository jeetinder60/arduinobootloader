//@see:
// https://github.com/arduino/ArduinoCore-avr/blob/master/bootloaders/atmega/ATmegaBOOT_168.c
// AVR061.pdf
// Licence can be viewed at http://www.fsf.org/licenses/gpl.txt
//
// Max FLASH is 64 KB !
//
// Tested with m324pa
//**********************************************************

#include <avr/eeprom.h>
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <inttypes.h>
#include <util/delay.h>

/* Use the F_CPU defined in Makefile */

/* set the waiting time for the bootloader */
/* get this from the Makefile instead */
#ifndef MAX_TIME_COUNT
#define MAX_TIME_COUNT (F_CPU >> 4)
#endif

/* After this many errors give up and launch application */
#define MAX_ERROR_COUNT 5

#ifndef NUM_LED_FLASHES
#define NUM_LED_FLASHES 5
#endif

/* set the UART baud rate */
#ifndef BAUD_RATE
#define BAUD_RATE 115200
#endif

/* SW_MAJOR and MINOR needs to be updated from time to time to avoid warning
 * message from AVR Studio */
/* never allow AVR Studio to do an update !!!! */
#define HW_VER 0x01
#define SW_MAJOR 0x01
#define SW_MINOR 0x00

/* onboard LED is used to indicate, that the bootloader was entered (flashing)
 */
//#define LED_DDR DDRB    //is defined in makefile
//#define LED_PORT PORTB
//#define LED PINB0

/* define various device id's */
/* manufacturer byte is always the same */
#define SIG1                                                                   \
  0x1E // Yep, Atmel is the only manufacturer of AVR micros.  Single source :(

#if defined __AVR_ATmega324PA__
#define SIG2 0x95
#define SIG3 0x11
#define PAGE_SIZE 0x40U // 64 words
#elif defined __AVR_ATmega644P__
#define SIG2 0x96
#define SIG3 0x0A
#define PAGE_SIZE 0x80U // 128 words
#elif defined __AVR_ATmega1284P__
#define SIG2 0x97
#define SIG3 0x05
#define PAGE_SIZE 0x80U // 128 words
#elif defined __AVR_ATmega328P__
#define SIG2	0x95
#define SIG3	0x0F
#define PAGE_SIZE	0x40U	//64 words
#endif

/* function prototypes */
void putch(char);
char getch(void);
void getNch(uint8_t);
void byte_response(uint8_t);
void nothing_response(void);
void countError(void);

/* some variables */
union address_union {
  uint16_t word;
  uint8_t byte[2];
} address;

union length_union {
  uint16_t word;
  uint8_t byte[2];
} length;

union TLengthUnion {
  uint16_t word;
  uint8_t byte[2];
};

uint8_t buff[256];

#define app_start() asm("jmp 0x000\n")

void main (void) __attribute__ ((naked)) __attribute__ ((section (".init0")));

uint8_t error_count;

/* main program starts here */
void main(void) {
  uint8_t ch;
  uint16_t w;

  asm("eor	r1, r1");
  SREG = 0;
  SP = 0x8ff;

  error_count = MAX_ERROR_COUNT;

  /* initialize UART(s) depending on CPU defined */
  UBRR0L = (uint8_t)((float)F_CPU/((float)BAUD_RATE*8.0)-1.0 + 0.5);
  UBRR0H = ((uint8_t)((float)F_CPU/((float)BAUD_RATE*8.0)-1.0 + 0.5)) >> 8;
  UCSR0A = _BV(U2X0);
  UCSR0C = 0x06;
  UCSR0B = _BV(TXEN0) | _BV(RXEN0);
  DDRD &= ~_BV(PIND0);
  PORTD &= ~_BV(PIND0);
  DDRD |= _BV(PIND1);
  PORTD |= _BV(PIND1);

  /* set LED pin as output */
  LED_DDR |= _BV(LED);
  LED_PORT &= ~_BV(LED);

  /* flash onboard LED to signal entering of bootloader */
  {
    register uint8_t count = 2 * NUM_LED_FLASHES;
    while (count--) {
      LED_PORT ^= _BV(LED);
      _delay_ms(100);
      if (UCSR0A & _BV(RXC0)) {
        break;
      }      
    }
  }

  /* forever loop */
  for (;;) {
    ch = getch();
    /* A bunch of if...else if... gives smaller code than switch...case ! */

    /* 0: Hello is anyone home ? */
    /* P: Enter programming mode  */
    /* R: Erase device, don't care as we will erase one page at a time anyway.*/
    /* Q: Leave programming mode  */

    if (ch == '0' || ch == 'P' || ch == 'R' || ch == 'Q') {
      nothing_response();
    }

    /* Request programmer ID */
    else if (ch == '1') {
      if (getch() == ' ') {
        putch(0x14);
        putch('A');
        putch('V');
        putch('R');
        putch(' ');
        putch('I');
        putch('S');
        putch('P');
        putch(0x10);
      } else {
        countError();
      }
    }

    /* AVR ISP/STK500 board commands  DON'T CARE so default nothing_response */
    else if (ch == '@') {
      if (getch() > 0x85) {
        getch();
      }
      nothing_response();
    }

    /* 0x41 Get Parameter Value*/
    else if (ch == 'A') {
      ch = getch();
      if (ch == 0x80)
        byte_response(HW_VER); // Hardware version
      else if (ch == 0x81)
        byte_response(SW_MAJOR); // Software major version
      else if (ch == 0x82)
        byte_response(SW_MINOR); // Software minor version
      else if (ch == 0x98)
        byte_response(
            0x03); // Unknown but seems to be required by avr studio 3.56
      else
        byte_response(
            0x00); // Covers various unnecessary responses we don't care about
    }

    /* Device Parameters  DON'T CARE, DEVICE IS FIXED  */
    else if (ch == 'B') {
      getNch(20);
      nothing_response();
    }

    /* Parallel programming stuff  DON'T CARE  */
    else if (ch == 'E') {
      getNch(4);
      nothing_response();
    }

    /* 0x55 Load Address */
    /* Set address, little endian. EEPROM in bytes, FLASH in words  */
    /* Perhaps extra address bytes may be added in future to support > 128kB
       FLASH.  */
    /* This might explain why little endian was used here, big endian used
       everywhere else.  */
    else if (ch == 'U') {
      address.byte[0] = getch();
      address.byte[1] = getch();
      address.word <<= 1;
      nothing_response();
    }

    /* Universal SPI programming command, disabled.  Would be used for fuses and
       lock bits.  */
    else if (ch == 'V') {
      if (getch() == 0x30) {
        getch();
        ch = getch();
        getch();
        if (ch == 0) {
          byte_response(SIG1);
        } else if (ch == 1) {
          byte_response(SIG2);
        } else {
          byte_response(SIG3);
        }
      } else {
        getNch(3);
        byte_response(0x00);
      }
    }

    /* Write memory, length is big endian and is in bytes  */
    else if (ch == 'd') {
      length.byte[1] = getch();
      length.byte[0] = getch();
      char memtype = getch();
      for (w = 0; w < length.word; w++) {
        buff[w] = getch(); // Store data in buffer, can't keep up with serial
                           // data stream whilst programming pages
      }
      if (getch() == ' ') {
        if (memtype == 'E') { // Write to EEPROM one byte at a time
          for (w = 0; w < length.word; w++) {
            eeprom_write_byte((void *)address.word, buff[w]);
            address.word++;
          }
        } else { // Write to FLASH one page at a time
          /* if ((length.byte[0] & 0x01) == 0x01) length.word++;	//Even
           * up an odd number of bytes */
          if ((length.byte[0] & 0x01))
            length.word++; // Even up an odd number of bytes
          while (bit_is_set(EECR, EEPE))
            ; // Wait for previous EEPROM writes to complete
          asm volatile(
              "clr	r17		\n\t" // page_word_count
              "lds	r30,address	\n\t" // Address of FLASH location (in
                                              // bytes)
              "lds	r31,address+1	\n\t"
              "ldi	r28,lo8(buff)	\n\t" // Start of buffer array in RAM
              "ldi	r29,hi8(buff)	\n\t"
              "lds	r24,length	\n\t" // Length of data to be written
                                              // (in bytes)
              "lds	r25,length+1	\n\t"
              "length_loop:		\n\t" // Main loop, repeat for number of
                                              // words in block
              "cpi	r17,0x00	\n\t" // If page_word_count=0 then erase
                                              // page
              "brne	no_page_erase	\n\t"
              "wait_spm1:		\n\t"
              "lds	r16,%0		\n\t" // Wait for previous spm to
                                              // complete
              "andi	r16,1           \n\t"
              "cpi	r16,1           \n\t"
              "breq	wait_spm1       \n\t"
              "ldi	r16,0x03	\n\t" // Erase page pointed to by Z
              "sts	%0,r16		\n\t"
              "spm			\n\t"
              "wait_spm2:		\n\t"
              "lds	r16,%0		\n\t" // Wait for previous spm to
                                              // complete
              "andi	r16,1           \n\t"
              "cpi	r16,1           \n\t"
              "breq	wait_spm2       \n\t"

              "ldi	r16,0x11	\n\t" // Re-enable RWW section
              "sts	%0,r16		\n\t"
              "spm			\n\t"
              "no_page_erase:		\n\t"
              "ld	r0,Y+		\n\t" // Write 2 bytes into page buffer
              "ld	r1,Y+		\n\t"

              "wait_spm3:		\n\t"
              "lds	r16,%0		\n\t" // Wait for previous spm to
                                              // complete
              "andi	r16,1           \n\t"
              "cpi	r16,1           \n\t"
              "breq	wait_spm3       \n\t"
              "ldi	r16,0x01	\n\t" // Load r0,r1 into FLASH page
                                              // buffer
              "sts	%0,r16		\n\t"
              "spm			\n\t"

              "inc	r17		\n\t" // page_word_count++
              "cpi r17,%1	        \n\t"
              "brlo	same_page	\n\t" // Still same page in FLASH
              "write_page:		\n\t"
              "clr	r17		\n\t" // New page, write current one
                                              // first
              "wait_spm4:		\n\t"
              "lds	r16,%0		\n\t" // Wait for previous spm to
                                              // complete
              "andi	r16,1           \n\t"
              "cpi	r16,1           \n\t"
              "breq	wait_spm4       \n\t"
              "ldi	r16,0x05	\n\t" // Write page pointed to by Z
              "sts	%0,r16		\n\t"
              "spm			\n\t"
              "wait_spm5:		\n\t"
              "lds	r16,%0		\n\t" // Wait for previous spm to
                                              // complete
              "andi	r16,1           \n\t"
              "cpi	r16,1           \n\t"
              "breq	wait_spm5       \n\t"
              "ldi	r16,0x11	\n\t" // Re-enable RWW section
              "sts	%0,r16		\n\t"
              "spm			\n\t"
              "same_page:		\n\t"
              "adiw	r30,2		\n\t" // Next word in FLASH
              "sbiw	r24,2		\n\t" // length-2
              "breq	final_write	\n\t" // Finished
              "rjmp	length_loop	\n\t"
              "final_write:		\n\t"
              "cpi	r17,0		\n\t"
              "breq	block_done	\n\t"
              "adiw	r24,2		\n\t" // length+2, fool above check on
                                              // length after short page write
              "rjmp	write_page	\n\t"
              "block_done:		\n\t"
              "clr	__zero_reg__	\n\t" // restore zero register
              : "=m"(SPMCSR)
              : "M"(PAGE_SIZE)
              : "r0", "r16", "r17", "r24", "r25", "r28", "r29", "r30", "r31");
          /* Should really add a wait for RWW section to be enabled, don't
           * actually need it since we never */
          /* exit the bootloader without a power cycle anyhow */
        }
        putch(0x14);
        putch(0x10);
      } else {
        countError();
      }
    }

    /* 0x74 Read Page */
    else if (ch == 't') {
      union TLengthUnion length;
      length.byte[1] = getch();
      length.byte[0] = getch();
      char memtype = getch();
      if (getch() == ' ') {             // Command terminator
        putch(0x14);
        for (w = 0; w < length.word;
             w++) {             // Can handle odd and even lengths okay
          if (memtype == 'E') { // Byte access EEPROM read
            putch(eeprom_read_byte((void *)address.word));
            address.word++;
          } else {
            putch(pgm_read_byte_near(address.word));
            address.word++;
          }
        }
        putch(0x10);
      }
    }

    /* Get device signature bytes  */
    else if (ch == 'u') {
      if (getch() == ' ') {
        putch(0x14);
        putch(SIG1);
        putch(SIG2);
        putch(SIG3);
        putch(0x10);
      } else {
        countError();
      }
    }

    /* Read oscillator calibration byte */
    else if (ch == 'v') {
      byte_response(0x00);
    } else {
      countError();
    }
  } /* end of forever loop */
}

void putch(char ch) {
  while (!(UCSR0A & _BV(UDRE0)))
    ;
  UDR0 = ch;
}

char getch(void) {
  uint32_t count = MAX_TIME_COUNT;
  while (!(UCSR0A & _BV(RXC0))) {
    if (--count == 0) {
      app_start();
    }
  }
  return UDR0;
}

void getNch(uint8_t count) {
  while (count--) {
    getch(); // need to handle time out
  }
}

void byte_response(uint8_t val) {
  if (getch() == ' ') {
    putch(0x14);
    putch(val);
    putch(0x10);
  } else {
    countError();
  }
}

void nothing_response(void) {
  if (getch() == ' ') {
    putch(0x14);
    putch(0x10);
  } else {
    countError();
  }
}

void countError(void) {
  if (--error_count == 0) {
    app_start();
  }
}
