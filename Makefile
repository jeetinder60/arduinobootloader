GCCDIR    = /opt/arduino-1.8.10/hardware/tools/avr/bin
CC        = $(GCCDIR)/avr-gcc
OBJCOPY   = $(GCCDIR)/avr-objcopy
OBJDUMP   = $(GCCDIR)/avr-objdump
SIZE      = $(GCCDIR)/avr-size
AVRDUDE   = $(GCCDIR)/avrdude
OBJDIR    = ./build/
AVRDUDE_CONFIG = ../../lib/AVR/avrdude.conf
ISPPORT   = /dev/avr910

MMCU      =
F_CPU     =
BAUD_RATE = 115200

all:

AVRCD_m324pa_20MHz: MMCU=atmega324pa
AVRCD_m324pa_20MHz: F_CPU=20000000L
AVRCD_m324pa_20MHz: SECTION_START=0x7C00
AVRCD_m324pa_20MHz: LED = -DLED_DDR=DDRB -DLED_PORT=PORTB -DLED=PINB0
AVRCD_m324pa_20MHz: BAUD_RATE = 38400
AVRCD_m324pa_20MHz: common

AVRCD_m324pa_20MHz_flash: MMCU=atmega324pa
AVRCD_m324pa_20MHz_flash: F_CPU=20000000L
AVRCD_m324pa_20MHz_flash: LFUSE = FF
AVRCD_m324pa_20MHz_flash: HFUSE = D4
AVRCD_m324pa_20MHz_flash: EFUSE = FC
AVRCD_m324pa_20MHz_flash: flash

AVRCD_m644p_20MHz: MMCU=atmega644p
AVRCD_m644p_20MHz: F_CPU=20000000L
AVRCD_m644p_20MHz: SECTION_START=0xFC00
AVRCD_m644p_20MHz: LED = -DLED_DDR=DDRB -DLED_PORT=PORTB -DLED=PINB0
AVRCD_m644p_20MHz: BAUD_RATE = 38400
AVRCD_m644p_20MHz: common

AVRCD_m644p_20MHz_flash: MMCU=atmega644p
AVRCD_m644p_20MHz_flash: F_CPU=20000000L
AVRCD_m644p_20MHz_flash: LFUSE = FF
AVRCD_m644p_20MHz_flash: HFUSE = D6
AVRCD_m644p_20MHz_flash: EFUSE = FC
AVRCD_m644p_20MHz_flash: flash

AVRCD_m1284p_20MHz: MMCU=atmega1284p
AVRCD_m1284p_20MHz: F_CPU=20000000L
AVRCD_m1284p_20MHz: SECTION_START=0x01FC00
AVRCD_m1284p_20MHz: LED = -DLED_DDR=DDRB -DLED_PORT=PORTB -DLED=PINB0
AVRCD_m1284p_20MHz: BAUD_RATE = 115200
AVRCD_m1284p_20MHz: common

AVRCD_m1284p_20MHz_flash: MMCU=atmega1284p
AVRCD_m1284p_20MHz_flash: F_CPU=20000000L
AVRCD_m1284p_20MHz_flash: LFUSE = FF
AVRCD_m1284p_20MHz_flash: HFUSE = D6
AVRCD_m1284p_20MHz_flash: EFUSE = FC
AVRCD_m1284p_20MHz_flash: flash

UNO_m328p_16MHz: MMCU=atmega328p
UNO_m328p_16MHz: F_CPU=16000000L
UNO_m328p_16MHz: SECTION_START=0x7C00
UNO_m328p_16MHz: LED = -DLED_DDR=DDRB -DLED_PORT=PORTB -DLED=PINB5
UNO_m328p_16MHz: BAUD_RATE = 115200
UNO_m328p_16MHz: common

UNO_m328p_16MHz_flash: MMCU=atmega328p
UNO_m328p_16MHz_flash: F_CPU=16000000L
UNO_m328p_16MHz_flash: LFUSE = FF
UNO_m328p_16MHz_flash: HFUSE = D4
UNO_m328p_16MHz_flash: EFUSE = 04
UNO_m328p_16MHz_flash: flash

NANO_m328p_16MHz_flash: UNO_m328p_16MHz_flash

common: ATmegaBOOT.c
	@echo _$(MMCU) _$(F_CPU)
	@mkdir	-p $(OBJDIR)
	@echo cc ATmegaBOOT.c
	@$(CC)	-g -Wall -Os -ffreestanding -ffunction-sections $(LED) -DBAUD_RATE=$(BAUD_RATE) -mmcu=$(MMCU) -DF_CPU=$(F_CPU) -c -o $(OBJDIR)BOOT_$(MMCU)_$(F_CPU).o ATmegaBOOT.c
	@echo link
	@$(CC) -nostartfiles -g -Wall -Os -mmcu=$(MMCU) -DF_CPU=$(F_CPU) -Wl,--relax,--gc-sections,--section-start=.text=$(SECTION_START) -o $(OBJDIR)BOOT_$(MMCU)_$(F_CPU).elf $(OBJDIR)BOOT_$(MMCU)_$(F_CPU).o
	@echo objcopy
	@$(OBJCOPY) -O ihex $(OBJDIR)BOOT_$(MMCU)_$(F_CPU).elf $(OBJDIR)BOOT_$(MMCU)_$(F_CPU).hex
	@echo objdump
	@$(OBJDUMP) -S $(OBJDIR)BOOT_$(MMCU)_$(F_CPU).elf > $(OBJDIR)BOOT_$(MMCU)_$(F_CPU).lst
	@echo size
	@$(SIZE) -C --mcu=$(MMCU) $(OBJDIR)BOOT_$(MMCU)_$(F_CPU).elf
	@echo

flash:
	$(AVRDUDE) -C $(AVRDUDE_CONFIG) -c avr910 -p $(MMCU) -P $(ISPPORT) -b 115200 -e -u -U efuse:w:0x$(EFUSE):m -U hfuse:w:0x$(HFUSE):m -U lfuse:w:0x$(LFUSE):m
	$(AVRDUDE) -C $(AVRDUDE_CONFIG) -c avr910 -p $(MMCU) -P $(ISPPORT) -b 115200 -U flash:w:$(OBJDIR)BOOT_$(MMCU)_$(F_CPU).hex -U lock:w:0x0f:m

clean:
	@echo clean
	@rm -rf $(OBJDIR)*.o $(OBJDIR)*.elf $(OBJDIR)*.lst $(OBJDIR)*.map $(OBJDIR)*.sym $(OBJDIR)*.lss $(OBJDIR)*.eep $(OBJDIR)*.srec $(OBJDIR)*.bin $(OBJDIR)*.hex
	@echo

readFUSE:
	$(AVRDUDE) -C $(AVRDUDE_CONFIG) -c avr910 -p atmega324p -F -P $(ISPPORT) -b 115200 -e -u -U efuse:r:-:h -U hfuse:r:-:h -U lfuse:r:-:h
